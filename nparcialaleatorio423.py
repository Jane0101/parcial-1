import simpy 
import random 
import math


n1 = 0
n2 = 0
n3 = 0

cont1 = 0
cont2 = 0
cont3 = 0


def simulacion(env):
    global cont1
    global cont2
    global cont3
    global n1
    global n2
    global n3
    dias = 0
    aleatorio = [4,2,3]
    while True:
        dias += 1
        print("..........Simulador de Trafico por Horarios Dia {0}".format(dias), "...................")
        print("HORARIO--MAÑANA--{0}".format(str(env.now)))
        d_mañana = random.choice(aleatorio)
        n1 = (random.randrange(10000))
        print(n1)
        cont1 += n1
        yield env.timeout(d_mañana)
        
        print("HORARIO--TARDE--{0}".format(str(env.now)))
        d_tarde = random.choice(aleatorio)
        n2 = (random.randrange(10000))
        print(n2)
        cont2 += n2
        yield env.timeout(d_tarde)
        
        print("HORARIO--NOCHE--{0}".format(str(env.now)))
        d_noche = random.choice(aleatorio)
        n3 = (random.randrange(10000))
        print(n3)
        cont3 += n3
        yield env.timeout(d_noche)
        


if __name__== '__main__':
    env = simpy.Environment()
    env.process(simulacion(env))
    env.run(until=279)
    total = math.floor(cont1 + cont2 + cont3)
    print("Trafico del horario de la mañana: ",cont1, " autos" )
    print("Trafico del horario de la tarde: ",cont2, " autos")
    print("Trafico del horario de la noche: ",cont3, " autos")
    print("El total de trafico de la simulacion es: ",total, "autos")
    print("Promedio del trafico entre las jornadas es de: ",math.floor(total/3))
    print("En el dia 15, el trafico de la jornada de la mañana es de: ",math.floor(cont1*15/31), " autos")
    

